#include "fft_getter.h"
#include "bass.h"
#include <QDebug>
#include <QProcess>
#include <stdio.h>
#include "keydriver.h"
#include "keydriverfake.h"

HRECORD chan;	// recording channel

// Recording callback - not doing anything with the data
BOOL CALLBACK DuffRecording(HRECORD handle, const void *buffer, DWORD length, void *user)
{
    Q_UNUSED(handle);
    Q_UNUSED(buffer);
    Q_UNUSED(length);
    Q_UNUSED(user);

    return TRUE; // continue recording
}


fft_getter::fft_getter()
{

}

fft_getter::fft_getter(int device)
{
    if (!BASS_RecordInit(device))
    {
        qDebug() << "init fail";
    }
}

fft_getter::~fft_getter()
{

}

bool fft_getter::open()
{

    // start recording (44100hz stereo 16-bit)
    if (!(chan=BASS_RecordStart(44100,2,MAKELONG(0,50),0,0)))
    {
        qDebug() << "record start fail";
        return false;
    }
//    keydriver->open();

    return true;

}

QString fft_getter::list()
{
    BASS_DEVICEINFO info;
    int i=0;
    while(BASS_GetDeviceInfo(i,&info)){
        std::cout << i << " " << info.name;
        i++;
    }
    return QString(info.name);
}

#define FFT_LENGTH 8192

QVector<int> fft_getter::getBins()
{
    QVector<int> vals;
    vals.fill(0,14);
    float fft[FFT_LENGTH/2];
    if(BASS_ChannelGetData(chan, fft, BASS_DATA_FFT8192) == -1)
    {
        qDebug() << "getdata fail";
    }
    else
    {
        for(int col=0;col<14;col++){
            vals[col] = 0;
            for(int i=0;i<(FFT_LENGTH/2)/14;i++){
                vals[col] += (int)(fft[(FFT_LENGTH/2)/28*col+i]*64);
            }

//            vals[col] +=(int)(fft[4*col]*64);
//            vals[col] +=(int)(fft[4*col+1]*64);
//            vals[col] +=(int)(fft[4*col+2]*64);
//            vals[col] +=(int)(fft[4*col+3]*64);

        }
    }
    return vals;
}

void fft_getter::setDriver(KeyDriver *driver)
{
    keydriver = driver;
}

void fft_getter::flushLeds()
{
    static int iteration = 0;
    QVector<int> vals = getBins();
    if(vals.isEmpty())
    {
        qDebug() << "getdata fail";
    }
    else
    {
        emit newVals(vals);
    }
    iteration++;
    qDebug() << iteration;
}

void fft_getter::flushValues()
{
//    static int iteration = 0;
//    QString fft_text;
//    QString line[4];
//    int vals[14];
//    getBins(vals);
//    QString str_value;
//    for(int col=0;col<14;col++){
//        str_value = QString::number(vals[col]);
//        fft_text.append(str_value);

//        for(int j=0;j<14-str_value.length();j++){
//            fft_text.append(" ");
//        }
//    }
//    fft_text.append("\n");
//    std::cout << fft_text.toStdString();
//    fft_text.clear();
//    qDebug() << iteration;
//    iteration++;
}


