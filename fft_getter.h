#ifndef FFT_GETTER_H
#define FFT_GETTER_H

#include <QObject>
#include <iostream>
#include <keydriver.h>
#include <QVector>

class fft_getter : public QObject
{
    Q_OBJECT
public:

    fft_getter();
    fft_getter(int device);
    ~fft_getter();
    bool open();
    static QString list();

    QVector<int> getBins();
    void setDriver(KeyDriver * driver);

   void flushLeds();
   void flushValues(void);

signals:
   void newVals(QVector<int> vals);

private:
   KeyDriver * keydriver;
};

#endif // FFT_GETTER_H
