#include "keydriver.h"
#include <QVector>
#include <QDebug>

KeyDriver::KeyDriver(QObject *parent) : QObject()
{
    key_names[0][0] =  "tilde";
    key_names[0][1] = "1";
    key_names[0][2] = "2";
    key_names[0][3] = "3";
    key_names[0][4] = "4";
    key_names[0][5] = "5";
    key_names[0][6] = "6";
    key_names[0][7] = "7";
    key_names[0][8] = "8";
    key_names[0][9] = "9";
    key_names[0][10] = "0";
    key_names[0][11] = "minus";
    key_names[0][12] = "equal";
    key_names[0][13] = "bachspace";

    key_names[1][0] =  "tab";
    key_names[1][1] = "q";
    key_names[1][2] = "w";
    key_names[1][3] = "e";
    key_names[1][4] = "r";
    key_names[1][5] = "t";
    key_names[1][6] = "y";
    key_names[1][7] = "u";
    key_names[1][8] = "i";
    key_names[1][9] = "o";
    key_names[1][10] = "p";
    key_names[1][11] = "open_bracket";
    key_names[1][12] = "close_bracket";
    key_names[1][13] = "slash";

    key_names[2][0] =  "caps_lock";
    key_names[2][1] = "a";
    key_names[2][2] = "s";
    key_names[2][3] = "d";
    key_names[2][4] = "f";
    key_names[2][5] = "g";
    key_names[2][6] = "h";
    key_names[2][7] = "j";
    key_names[2][8] = "k";
    key_names[2][9] = "l";
    key_names[2][10] = "semicolon";
    key_names[2][11] = "quote";
    key_names[2][12] = "backslash";
    key_names[2][13] = "enter";

    key_names[3][0] =  "shift_left";
    key_names[3][1] = "backslash";
    key_names[3][2] = "z";
    key_names[3][3] = "x";
    key_names[3][4] = "c";
    key_names[3][5] = "v";
    key_names[3][6] = "b";
    key_names[3][7] = "b";
    key_names[3][8] = "n";
    key_names[3][9] = "m";
    key_names[3][10] = "comma";
    key_names[3][11] = "period";
    key_names[3][12] = "slash";
    key_names[3][13] = "shift_right";

    keys[0][0] =  LedKeyboard::Key::tilde;
    keys[0][1] = LedKeyboard::Key::n1;
    keys[0][2] = LedKeyboard::Key::n2;
    keys[0][3] = LedKeyboard::Key::n3;
    keys[0][4] = LedKeyboard::Key::n4;
    keys[0][5] = LedKeyboard::Key::n5;
    keys[0][6] = LedKeyboard::Key::n6;
    keys[0][7] = LedKeyboard::Key::n7;
    keys[0][8] = LedKeyboard::Key::n8;
    keys[0][9] = LedKeyboard::Key::n9;
    keys[0][10] = LedKeyboard::Key::n0;
    keys[0][11] = LedKeyboard::Key::minus;
    keys[0][12] = LedKeyboard::Key::equal;
    keys[0][13] = LedKeyboard::Key::backspace;


    keys[1][0] = LedKeyboard::Key::tab;
    keys[1][1] = LedKeyboard::Key::q;
    keys[1][2] = LedKeyboard::Key::w;
    keys[1][3] = LedKeyboard::Key::e;
    keys[1][4] = LedKeyboard::Key::r;
    keys[1][5] = LedKeyboard::Key::t;
    keys[1][6] = LedKeyboard::Key::y;
    keys[1][7] = LedKeyboard::Key::u;
    keys[1][8] = LedKeyboard::Key::i;
    keys[1][9] = LedKeyboard::Key::o;
    keys[1][10] = LedKeyboard::Key::p;
    keys[1][11] = LedKeyboard::Key::open_bracket;
    keys[1][12] = LedKeyboard::Key::close_bracket;
    keys[1][13] = LedKeyboard::Key::slash;

    keys[2][0] = LedKeyboard::Key::caps_lock;
    keys[2][1] = LedKeyboard::Key::a;
    keys[2][2] = LedKeyboard::Key::s;
    keys[2][3] = LedKeyboard::Key::d;
    keys[2][4] = LedKeyboard::Key::f;
    keys[2][5] = LedKeyboard::Key::g;
    keys[2][6] = LedKeyboard::Key::h;
    keys[2][7] = LedKeyboard::Key::j;
    keys[2][8] = LedKeyboard::Key::k;
    keys[2][9] = LedKeyboard::Key::l;
    keys[2][10] = LedKeyboard::Key::semicolon;
    keys[2][11] = LedKeyboard::Key::quote;
    keys[2][12] = LedKeyboard::Key::backslash;
    keys[2][13] = LedKeyboard::Key::enter;

//    keys[3][0] = LedKeyboard::Key::shift_left;
//    keys[3][1] = LedKeyboard::Key::backslash;
//    keys[3][2] = LedKeyboard::Key::z;
//    keys[3][3] = LedKeyboard::Key::x;
//    keys[3][4] = LedKeyboard::Key::c;
//    keys[3][5] = LedKeyboard::Key::v;
//    keys[3][6] = LedKeyboard::Key::b;
//    keys[3][7] = LedKeyboard::Key::b;
//    keys[3][8] = LedKeyboard::Key::n;
//    keys[3][9] = LedKeyboard::Key::m;
//    keys[3][10] = LedKeyboard::Key::comma;
//    keys[3][11] = LedKeyboard::Key::period;
//    keys[3][12] = LedKeyboard::Key::intl_backslash;
//    keys[3][13] = LedKeyboard::Key::shift_right;


    keys[3][0] = LedKeyboard::Key::shift_left;
    keys[3][1] = LedKeyboard::Key::z;
    keys[3][2] = LedKeyboard::Key::x;
    keys[3][3] = LedKeyboard::Key::c;
    keys[3][4] = LedKeyboard::Key::v;
    keys[3][5] = LedKeyboard::Key::b;
    keys[3][6] = LedKeyboard::Key::b;
    keys[3][7] = LedKeyboard::Key::n;
    keys[3][8] = LedKeyboard::Key::m;
    keys[3][9] = LedKeyboard::Key::comma;
    keys[3][10] = LedKeyboard::Key::period;
    keys[3][11] = LedKeyboard::Key::intl_backslash;
    keys[3][12] = LedKeyboard::Key::shift_right;
    keys[3][13] = LedKeyboard::Key::backslash;

    for(int row=0;row<ROWS;row++){
        for(int col=0;col<COLS;col++){
            keyboard[row][col] = false;
        }
    }

}

KeyDriver::~KeyDriver()
{

}

LedKeyboard::Key KeyDriver::getKeyFromPos(int row, int col)
{
    return keys[row][col];
}

void KeyDriver::repaint(QVector<int> vals)
{
    for(int col=0;col<14;col++){
        for(int row=0;row<4;row++){
            setLed(3-row,col,vals[col]>20+row*20);
        }
    }
    commit();
}

