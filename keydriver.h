#ifndef KEYDRIVER_H
#define KEYDRIVER_H

#include <QObject>
#include <QString>
#include "Keyboard.h"

#define ROWS    4
#define COLS    14

class KeyDriver : public QObject
{
        Q_OBJECT
public:
    explicit KeyDriver(QObject *parent = nullptr);
    ~KeyDriver();
    virtual bool setLed(LedKeyboard::Key key, bool state) = 0;
    virtual bool setLed(int row, int col, bool state) = 0;
    virtual bool commit(void) = 0;
    virtual void list(void) = 0;
    LedKeyboard::Key getKeyFromPos(int row, int col);

public slots:
    void repaint(QVector<int> vals);

protected:
    LedKeyboard::Key keys[4][14];
    QString key_names[4][14];
    bool keyboard[4][14];
};

#endif // KEYDRIVER_H
