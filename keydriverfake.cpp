#include "keydriverfake.h"
#include <iostream>

KeyDriverFake::KeyDriverFake()
{

}

bool KeyDriverFake::setLed(LedKeyboard::Key keyname, bool state)
{

    return true;
}

bool KeyDriverFake::setLed(int row, int col, bool state)
{
    keyboard[row][col] = state;
}

bool KeyDriverFake::commit()
{
    for(int row=0;row<ROWS;row++){
        for(int col=0;col<COLS;col++){
            if(keyboard[row][col]){
                std::cout << key_names[row][col].toStdString();
                for(int i=0;i<14-key_names[row][col].length();i++){
                    std::cout << " ";
                }
            } else {
                for(int i=0;i<14;i++){
                    std::cout << " ";
                }
            }
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    return true;
}

void KeyDriverFake::list()
{
    std::cout << "\nFake driver, nothing to list\n";
}
