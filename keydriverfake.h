#ifndef KEYDRIVERFAKE_H
#define KEYDRIVERFAKE_H
#include "keydriver.h"
#include "Keyboard.h"


class KeyDriverFake : public KeyDriver
{
public:
    KeyDriverFake();
    bool setLed(LedKeyboard::Key keyname, bool state);
    bool setLed(int row, int col, bool state);
    bool commit(void);
    void list(void);

private:
};

#endif // KEYDRIVERFAKE_H
