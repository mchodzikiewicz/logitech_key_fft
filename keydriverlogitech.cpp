#include "keydriverlogitech.h"
#include <iostream>

KeyDriverLogitech::KeyDriverLogitech()
{
    ledkeyboard = new LedKeyboard;
    ledkeyboard->open();
    LedKeyboard::Color color;
    color.red = 255;
    color.green = 255;
    color.blue = 255;
    ledkeyboard->setAllKeys(color);
}

bool KeyDriverLogitech::setLed(LedKeyboard::Key key, bool state)
{
    LedKeyboard::KeyValue val;
    LedKeyboard::Color color;
    val.key = key;
    if(state){
        val.color.red = 255;
        val.color.green = 255;
        val.color.blue = 255;
    } else {
        val.color.red = 0;
        val.color.green = 0;
        val.color.blue = 0;
    }
    return ledkeyboard->setKey(val);;
}

bool KeyDriverLogitech::setLed(int row, int col, bool state)
{
    setLed(keys[row][col],state);
}

bool KeyDriverLogitech::commit()
{
    return ledkeyboard->commit();
}

void KeyDriverLogitech::list()
{
    LedKeyboard::DeviceInfo info =  ledkeyboard->getCurrentDevice();
    std::cout << "\nKeyboard info:";
    std::cout << info.product;
    std::cout << "\n";
}

