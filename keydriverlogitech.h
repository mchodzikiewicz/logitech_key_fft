#ifndef KEYDRIVERLOGITECH_H
#define KEYDRIVERLOGITECH_H
#include <QString>
#include "keydriver.h"
#include "Keyboard.h"

class KeyDriverLogitech : public KeyDriver
{
public:
    KeyDriverLogitech();

    bool setLed(LedKeyboard::Key keyname, bool state);
    bool setLed(int row, int col, bool state);
    bool commit(void) ;
    void list(void);

private:
    LedKeyboard * ledkeyboard;

};

#endif // KEYDRIVERLOGITECH_H
