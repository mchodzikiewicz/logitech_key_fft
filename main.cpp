#include <QCoreApplication>
#include <QDebug>
#include "bass.h"
#include <QTimer>
#include "fft_getter.h"
#include "keydriverfake.h"
#include "keydriverlogitech.h"
#include <QObject>


bool fake_mode=false;
bool value_mode=false;
QString key_led_driver;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int device=0;
    if(argc){
        if(QString(argv[1]) == "-d"){
            device = atoi(argv[2]);
            if(QString(argv[3]) == "-f"){
                fake_mode = true;
            } else if(QString(argv[3]) == "-k"){ //keyboard type
                fake_mode = false;
            } else if(QString(argv[3]) == "-v"){
                value_mode = true;
            }
        } else if(QString(argv[1]) == "-h"){
            qDebug() << "fft_dumper first arg options:\n-h - help\n-l - list available devices\n-d [number] -select device\nif -d, next possible arg:\n-f - fake keyboard displayed in command line";
            return 0;
        }
    }
    fft_getter getter(device);
    KeyDriverLogitech keyboard;
    if(QString(argv[1]) == "-l"){
        fft_getter::list();
        keyboard.list();
        return 0;
    }
    qDebug() << "Chosen device: " << device;

    if(fake_mode && !value_mode){
        getter.setDriver(new KeyDriverFake);
    } else if(!fake_mode){
        getter.setDriver(&keyboard);
    }

    QObject::connect(&getter,SIGNAL(newVals(QVector<int>)),&keyboard,SLOT(repaint(QVector<int>)));
    getter.open();
    QEventLoop loop;
    while(1){
//        QTimer::singleShot(20,&loop,SLOT(quit()));
//        loop.exec();
        if(value_mode){
            getter.flushValues();
        } else {
            getter.flushLeds();
        }
    }
    return 0;// a.exec();
}

